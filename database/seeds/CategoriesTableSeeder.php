<?php

use App\Http\Models\CategoryModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Детектив',
            'История',
            'Классика',
            'Фантастика'
        ];


        foreach ($categories as $val) {
            $category = new CategoryModel([
                'user_id' => 1,
                'title' => $val
            ]);
            $category->save();
        }

        foreach ($categories as $val) {
            $category = new CategoryModel([
                'user_id' => 2,
                'title' => $val
            ]);
            $category->save();
        }
    }
}
