<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    public $table = 'categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'title'
    ];

    public function book()
    {
        return $this->belongsTo('App\Http\Models\BookModel', 'categories_id', 'id');
    }
}
