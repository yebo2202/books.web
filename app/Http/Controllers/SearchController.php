<?php

namespace App\Http\Controllers;

use App\Http\Models\AuthorModel;
use App\Http\Models\BookModel;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request['search'];
        $data = BookModel::where('title', 'LIKE', '%' . $keyword . '%')
            ->orWhereHas('author', function ($query) use($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%');
            })->with('author', 'queue')->get();
        return $data;
    }
}
