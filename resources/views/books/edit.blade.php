@extends('layouts.app')

@section('content')

    <br>
    <div class="col-lg-12">
        @if (Session::has('message'))
            <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
        @endif
        @if (Session::has('message_danger'))
            <div class="alert alert-danger fs-13">{{ Session::get('message_danger') }}</div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>

    <div id="app">
        <book-edit-component
                :current-book='{{ $book }}'
                :exist-authors='{{ $authors }}'
                :exist-category='{{ $categories }}'>
        </book-edit-component>
    </div>

    <script src="{{ asset('/js/app.js') }}"></script>

@endsection