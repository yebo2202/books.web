<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BookModel extends Model
{
    public $table = 'books';
    protected $primaryKey = 'id';
    protected $fillable = [
        'category_id',
        'users_id',
        'description',
        'filename',
        'image',
        'pages',
        'title',
        'year',
    ];

    public function author()
    {
        return $this->belongsToMany( 'App\Http\Models\AuthorModel', 'book_author', 'book_id', 'author_id', 'id')->withTimestamps();
    }
    public function category()
    {
        return $this->hasOne('App\Http\Models\CategoryModel', 'id', 'category_id');
    }
    public function queue()
    {
        return $this->belongsTo('App\Http\Models\QueueModel', 'id', 'book_id');
    }
}
