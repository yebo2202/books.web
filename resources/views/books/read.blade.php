@extends('layouts.app')

@section('content')

    <script src="{{asset('/js/pdfobject.js')}}"></script>
    <script>PDFObject.embed("/storage/{{$book->filename}}");</script>

@endsection