<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'integer|max:10',
            'title' => 'string|max:255'
        ];
    }

    public function messages()
    {
        return [
            'user_id.integer' => 'Да Вы хакер, сударь, раз смогли вместо числа (user_id) прислать строку!',
            'title.max' => 'Название категории должно содержать не более 255 символов'
        ];
    }
}
