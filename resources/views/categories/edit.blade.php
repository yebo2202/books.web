@extends('layouts.app')

@section('content')

    <div class="container">

        <br>

        <div class="col-lg-12 my-4 fs-20">
                   <a class="mr-2" href="{{ asset('/setting') }}">Настройки</a><i class="fas fa-angle-right fs-20"></i>
            <small><a class="mr-2" href="{{ asset('/setting/categories') }}">Категории</a></small><i class="fas fa-angle-right fs-20"></i>
            <small>Редактирование</small>
        </div>

        <div class="col-lg-10">
            @if (Session::has('message'))
                <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('message_danger'))
                <div class="alert alert-danger fs-13">{{ Session::get('message_danger') }}</div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <form class="form-inline" action="{{ asset('/category/update/'.$category['id']) }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" class="col-lg-10 form-control" name="title" value="{{ $category['title'] }}">
                        <button type="submit" class="col-lg-2 btn btn-outline-secondary">Обновить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
