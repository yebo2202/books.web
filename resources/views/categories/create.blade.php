@extends('layouts.app')

@section('content')

    <div class="container">

        <br>

        <div class="col-lg-3 my-4 fs-20">Категории
            <i class="fas fa-angle-right fs-20"></i>
            <small>Добавить</small>
        </div>

        <div class="col-lg-10">
            @if (Session::has('message'))
                <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('message_danger'))
                <div class="alert alert-danger fs-13">{{ Session::get('message_danger') }}</div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <form class="form-inline" action="{{ asset('/category/store') }}" method="post">
                        {{ csrf_field() }}
                        <input type="text" class="col-lg-10 form-control" name="title" placeholder="Название категории">
                        <button type="submit" class="col-lg-2 btn btn-outline-secondary">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection