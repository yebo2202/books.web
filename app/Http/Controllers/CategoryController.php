<?php

namespace App\Http\Controllers;

use App\Http\Models\CategoryModel;
use App\Http\Requests\CategoryStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function create()
    {
        return view('categories.create');
    }

    public function store(CategoryStoreRequest $request)
    {
        $category = new CategoryModel();
        $category->user_id = Auth::id();
        $category->title = $request['title'];
        $category->save();

        return back()->with('message', 'Категория успешно добавлена');
    }

    public function edit($id)
    {
        $category = CategoryModel::find($id);

        return view('categories.edit', compact('category'));
    }

    public function destroy($id)
    {
        $category = CategoryModel::find($id);

        if ($category->user_id == Auth::id()) {
            CategoryModel::where('id', $id)->delete();
        }

        return redirect('/setting/categories')->with('message', 'Категория успешно удалена');
    }

    public function update(Request $request, $id)
    {
        $category = CategoryModel::find($id);

        $category->update([
            'title' => $request['title'],
        ]);

        return redirect('/setting/categories')->with('message', 'Категория успешно обновлена');
    }
}
