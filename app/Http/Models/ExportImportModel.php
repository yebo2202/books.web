<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ExportImportModel extends Model
{
    public $table = 'export_import';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'type_of_action'
    ];
}
