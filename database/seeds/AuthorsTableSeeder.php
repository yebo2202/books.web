<?php

use App\Http\Models\AuthorModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            'Жюль Верн',
            'Артур Конан Дойл',
            'Александр Пушкин',
            'Лев Толстой'
        ];

        foreach ($authors as $item) {
            $author = new AuthorModel([
                'user_id' => 1,
                'name' => $item
            ]);
            $author->save();
        }
        foreach ($authors as $item) {
            $author = new AuthorModel([
                'user_id' => 2,
                'name' => $item
            ]);
            $author->save();
        }

    }
}
