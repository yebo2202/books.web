@extends('layouts.app')

@section('content')

    <!-- Контент страницы -->
    <div class="container">

        <br>

        <div class="col-lg-12">
            @if (Session::has('message'))
                <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('message_danger'))
                <div class="alert alert-danger fs-13">{{ Session::get('message_danger') }}</div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <!-- Текущий путь -->
        <div class="col-lg-12 my-4 fs-20">Книги
            <i class="fas fa-angle-right fs-20"></i>
            <small>Детальнее</small>
        </div>
        <!-- /Текущий путь -->

        <!-- Детальная информация о книге -->
        <div class="row">

            <div class="col-lg-4">
                <img class="img-fluid" src="/images/photos/{{ $book['image'] }}" alt="">
            </div>

            <div class="col-lg-6 offset-lg-1">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="{{ asset('/storage/'.$book['id'].'?file='.$book['filename'].$book['param']).'&title=111' }}">
                            <button type="button" @if (empty($book['filename'])) disabled @endif class="btn btn-outline-secondary">Читать</button>
                        </a>
                        <a href="{{ asset('/book/edit/'.$book['id']) }}">
                            <button type="button" class="btn btn-outline-secondary">Редактировать</button>
                        </a>
                        <a href="{{ asset('/queue/add/'.$book['id']) }}">
                            <button type="button" class="btn btn-outline-secondary">В очередь</button>
                        </a>

                        <button data-toggle="modal" data-target="#myModal" type="button" class="btn btn-outline-danger">Удалить</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 no-gutters"><h4 class="my-3">Характеристики книги</h4></div>
                </div>
                <div class="row fs-15">
                    <div class="col-lg-8">Дата добавления</div>
                    <div class="col-lg-4">{{ $book['created_at'] }}</div>
                </div>
                <div class="row fs-15">
                    <div class="col-lg-8">Название</div>
                    <div class="col-lg-4 detail-book-title fs-16">{{ $book['title'] }}</div>
                </div>

                @if( $book['author'] )
                    <div class="row fs-15">
                        <div class="col-lg-8">Автор: </div>
                        <div class="col-lg-4 card-author">
                            @foreach( $book['author'] as $author )
                                <a href="/author/{{ $author['id'] }}">{{ $author['name'] }}</a><br>
                            @endforeach
                        </div>
                    </div>
                @endif

                @if( $book['pages'] )
                    <div class="row fs-15">
                        <div class="col-lg-8">Количество страниц: </div>
                        <div class="col-lg-4">{{ $book['pages'] }} стр </div>
                    </div>
                @endif

                @if( $book['year'] )
                    <div class="row fs-15">
                        <div class="col-lg-8">Год издания:</div>
                        <div class="col-lg-4">{{ $book['year'] }}</div>
                    </div>
                @endif

                @if( $book['category'] )
                    <div class="row fs-15">
                        <div class="col-lg-8">Категория:</div>
                        <div class="col-lg-4"><a href="/category/{{ $book['category']['id'] }}">{{ $book['category']['title'] }}</a></div>
                    </div>
                @endif

                <br>
                @if( $book['param'] )
                    <?php
                        $param = explode("&", $book['param']);
                        $current_page = explode("=", $param[0]);
                        $progress = ceil(( 100 * $current_page[1] ) / $book['pages']);
                    ?>
                    <div class="row fs-15">
                        <div class="col-lg-8">Статистика: <b>{{$progress}}%</b></div>
                        <div class="col-lg-4">
                            <div class="progress" title="{{$progress}}%">
                                <div class="progress-bar" role="progressbar" style="width: {{$progress}}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>

                    </div>
                @endif
            </div>

        </div>
        <!-- /Детальная информация о книге -->

        <!-- Описание книги -->
        @if( $book['description'] )
            <h4 class="my-4">Описание книги "{{ $book['title'] }}"</h4>
            <div class="row">
                <p>{{ $book['description'] }}</p>
            </div>
        @endif
        <!-- /Описание книги -->

        <!-- Модальное окно -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Контент модального окна -->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Подтвердите</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p>Вы уверены что хотите удалить данную книгу?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Отмена</button>
                        <a href="{{ asset('/book/destroy/'.$book['id']) }}">
                            <button type="button" class="btn btn-outline-danger">Да, удалить</button>
                        </a>
                    </div>
                </div>
                <!-- /Контент модального окна -->

            </div>
        </div>
        <!-- /Модальное окно -->

    </div>
    <!-- /Контент страницы -->

@endsection