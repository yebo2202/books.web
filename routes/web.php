<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['auth']], function () {

    /*
    |--------------------------------------------------------------------------
    | Books
    |--------------------------------------------------------------------------
    */
    Route::get('/',                     'BookController@index');
    Route::get('/book/detail/{id}',     'BookController@detail');
    Route::get('/book/destroy/{id}',    'BookController@destroy');
    Route::get('/book/create',          'BookController@create');
    Route::get('/book/edit/{id}',       'BookController@edit');
    // Нестыковка в имени route и method вызвана необходимостью совместить pdf просмотрщик и laravel.
    Route::get('/storage/{id}',         'BookController@read');
    Route::post('/book/store',          'BookController@store');
    Route::post('/book/update/{id}',    'BookController@update');
    Route::post('/book/param/{id}',     'BookController@param');
    /*
    |--------------------------------------------------------------------------
    | Authors
    |--------------------------------------------------------------------------
    */
    Route::get('/author/create',        'AuthorController@create');
    Route::get('/author/destroy/{id}',  'AuthorController@destroy');
    Route::get('/author/edit/{id}',     'AuthorController@edit');
    Route::get('/author/{id}',          'AuthorController@list');
    Route::post('/author/store',        'AuthorController@store');
    Route::post('/author/update/{id}',  'AuthorController@update');
    /*
    |--------------------------------------------------------------------------
    | Category
    |--------------------------------------------------------------------------
    */
    Route::get('/category/create',      'CategoryController@create');
    Route::get('/category/destroy/{id}','CategoryController@destroy');
    Route::get('/category/edit/{id}',   'CategoryController@edit');
    Route::post('/category/store',      'CategoryController@store');
    Route::post('/category/update/{id}','CategoryController@update');
    /*
    |--------------------------------------------------------------------------
    | Queue
    |--------------------------------------------------------------------------
    */
    Route::get('/queue',                'QueueController@index');
    Route::get('/queue/add/{id}',       'QueueController@add');
    Route::get('/queue/del/{id}',       'QueueController@del');

    /*
    |--------------------------------------------------------------------------
    | Setting
    |--------------------------------------------------------------------------
    */
    Route::get('/setting/',             'SettingController@index');
    Route::get('/setting/export',       'SettingController@export')->name('export');

    /*
    |--------------------------------------------------------------------------
    | Search
    |--------------------------------------------------------------------------
    */
    Route::post('/search',              'SearchController@index');
    
    /*
    |--------------------------------------------------------------------------
    | Logout from app
    |--------------------------------------------------------------------------
    */
    Route::get('/logout',                      function () {
        Auth::logout();
        return redirect('/login');
    });
});


Auth::routes();
