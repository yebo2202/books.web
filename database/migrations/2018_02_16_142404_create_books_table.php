<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->nullable()->unsigned();
            $table->integer('user_id')->nullable()->unsigned();
            $table->text('description')->nullable();
            $table->string('filename')->nullable();
            $table->string('image')->default('no_image.png');
            $table->integer('pages')->nullable()->unsigned();
            $table->string('param')->nullable();
            $table->string('title');
            $table->integer('year')->nullable()->unsigned();
            $table->timestamps();
        });

        // Связь с остальными таблицами.
        Schema::table('books', function(Blueprint $table)
        {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
