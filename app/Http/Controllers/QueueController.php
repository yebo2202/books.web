<?php

namespace App\Http\Controllers;

use App\Http\Models\BookModel;
use App\Http\Models\QueueModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QueueController extends Controller
{
    public function index()
    {

        $queues = QueueModel::with('book', 'author', 'book.category')
            ->where('user_id', Auth::id())
            ->orderBy('id', 'asc')
            ->get()
            ->toArray();
        return view('queue.list', compact('queues'));
    }

    public function add($id)
    {
        $book = BookModel::find($id);
        if ($book['user_id'] == Auth::id()) {
            if (QueueModel::where('book_id', $id)->count() == '0') {
                $queue = new QueueModel();
                $queue->user_id = Auth::id();
                $queue->book_id = $id;
                $queue->save();
                return back()->with('message', 'Книга поставлена в очередь');
            } else {
                return back()->with('message', 'Такая книга уже есть в очереди');
            }
        }
        return abort(404);
    }

    public function del($id)
    {
        $book = BookModel::find($id);
        if ($book['user_id'] == Auth::id()) {
            QueueModel::where('book_id', $id)->delete();
            return back()->with('message', 'Книга успешно удалена из очереди');
        }
        return abort(404);
    }
}
