<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthorStoreRequest;
use App\Http\Models\AuthorModel;
use App\Http\Models\BookModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthorController extends Controller
{
    public function create()
    {
        return view('authors.create');
    }

    public function store(AuthorStoreRequest $request)
    {

        $author = new AuthorModel();
        $author->user_id = Auth::id();
        $author->name = $request['name'];
        $author->save();

        return back()->with('message', 'Автор успешно добавлен');
    }

    public function edit($id)
    {
        $author = AuthorModel::find($id);

        return view('authors.edit', compact('author'));
    }

    public function destroy($id)
    {
        $author = AuthorModel::find($id);
        if ( !empty($author->book()->count()) || $author->book()->count() > 0) {
            $author->book()->detach();
        }
        AuthorModel::where('id', $id)->delete();

        return redirect('/setting/authors')->with('message', 'Автор успешно удален');
    }

    public function update(Request $request, $id)
    {
        $author = AuthorModel::find($id);

        $author->update([
            'name' => $request['name'],
        ]);

        return redirect('/setting/authors')->with('message', 'Автор успешно обновлен');
    }

}
