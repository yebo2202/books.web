<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class QueueModel extends Model
{
    public $table = 'queue';
    protected $primaryKey = 'id';
    protected $fillable = [
        'book_order',
        'book_id'
    ];

    public function book()
    {
        return $this->hasOne('App\Http\Models\BookModel', 'id', 'book_id');
    }
    public function author()
    {
        return $this->belongsToMany('App\Http\Models\AuthorModel', 'book_author', 'book_id','author_id', 'book_id')->withTimestamps();
    }
}
