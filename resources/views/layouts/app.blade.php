<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title -->
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/2-col-portfolio.css') }}" rel="stylesheet">
    {{-- Style CSS --}}
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <!-- JS -->
    <script src="{{ asset('/js/fontawesome-all.js') }}" defer></script>

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container fs-14">
            <a class="navbar-brand" href="/">Books.web</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active"><a class="nav-link" href="{{ asset('/') }}">Главная<span class="sr-only">(current)</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="{{ asset('/queue') }}">Очередь</a></li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Добавить
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ asset('/author/create') }}">Автора</a>
                            <a class="dropdown-item" href="{{ asset('/category/create') }}">Категорию</a>
                            <a class="dropdown-item" href="{{ asset('/book/create') }}">Книгу</a>
                        </div>
                    </li>
                    {{-- Меню пользователя --}}
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @if(\Illuminate\Support\Facades\Auth::check())
                                {{ ucfirst(Illuminate\Support\Facades\Auth::user()->name) }}
                            @endif
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ asset('/setting') }}">Настройки</a>
                            <hr>
                            <a class="dropdown-item" href="{{ asset('/logout') }}">Выход</a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- Подвал. Подключаю только на главной странице -->
    @if(Request::is('/'))
        <div class="footer">
            <div class="container fs-14">
                <br>
                <div class="row">
                    <div class="col-md-6 footer_link">
                        <a href="#">О проекте</a><br>
                        <a href="#">Обратная связь</a>
                    </div>
                    <div class="col-md-6 right_col">
                        <div class="col-md-4 offset-8">
                            <div class="amount_title">Всего книг -</div>
                            <div class="amount_books">{{ count($books) }}</div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-lg-12">
                    <p class="m-0 text-center text-white fs-13">Copyright © Domenion.pp.ua 2018</p>
                </div>
            </div>
        </div>
    @endif

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.bundle.js') }}"></script>

    <!-- Scripts -->
    @yield('js')

</body>
</html>
