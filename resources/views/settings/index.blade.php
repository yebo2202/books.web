@extends('layouts.app')

@section('content')

    <div class="container">

        {{--{{ dd($tab) }}--}}
        <br>
        <div class="col-lg-12">
            @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('message_danger'))
                <div class="alert alert-danger">{{ Session::get('message_danger') }}</div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-lg-3 my-4 fs-20">Настройки
            <i class="fas fa-angle-right fs-20"></i>
            <small>Общие</small>
        </div>

        <!-- Tab links -->
        <div class="col-lg-12">
            <ul class="nav nav-tabs">
                <li class="nav-item"><a data-toggle="tab" class="nav-link active" href="#general">Общие</a></li>
                <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#authors">Авторы</a></li>
                <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#categories">Категории</a></li>
                <li class="nav-item"><a data-toggle="tab" class="nav-link" href="#tags">Теги</a></li>
            </ul>
        </div>

        <!-- Tab content -->

        <div class="tab-content">

            <div id="general" class="tab-pane active">
                <br>
                <div class="col-lg-12" title="Экспорт библиотеки">
                    <label class="col-lg-12" for="action_item_export"><b>Экспорт</b> библиотеки</label>
                    <div class="col-lg-12 no-gutters">
                        <div class="col-lg-2" id="action_item_export" style="float: left">
                            <a href="{{ route('export') }}" class="btn btn-outline-success">Экспорт</a>
                        </div>
                        @if($export)
                            <div class="col-lg-4" style="float: left">
                                <div class="last-export">Последний экспорт:
                                    <b>{{ Carbon\Carbon::parse($export['updated_at'])->format('d F, H:i')}}</b>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <br>
                <br>
                <hr>
                <div class="col-lg-12" title="Импорт библиотеки">
                    <label class="col-lg-12" for="action_item_import"><b>Импорт</b> библиотеки</label>
                    <div class="col-lg-12 no-gutters">
                        <div class="col-lg-2" id="action_item_import" style="float: left">
                            <a href="{{ route('export') }}" class="btn btn-outline-primary">Импорт</a>
                        </div>
                        @if($import)
                            <div class="col-lg-3" style="float: left">
                                <div class="last-export">Последний импорт:
                                    <b>{{ Carbon\Carbon::parse($import['updated_at'])->format('d F, H:i')}}</b>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div id="authors" class="tab-pane fade">
                <div class="col-lg-12">
                    <br>
                    @foreach( $authors as $key => $author)
                        <div class="row">
                            <div class="col-lg-1 float-left author_key">{{ $key + 1 }}</div>
                            <div class="col-md-4 float-left author_name">{{ $author['name'] }}</div>
                            <div class="col-md-2 float-left author_action">
                                <a href="{{ asset('/author/edit/'.$author['id']) }}"><i class="fas fa-pencil-alt author_edit"></i></a>
                                <a href="{{ asset('/author/destroy/'.$author['id']) }}"><i class="far fa-times-circle author_delete"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div id="categories" class="tab-pane fade">
                <div class="col-lg-12">
                    <br>
                    @foreach( $categories as $key => $category)
                        <div class="row">
                            <div class="col-lg-1 float-left category_key">{{ $key + 1 }}</div>
                            <div class="col-md-4 float-left category_name">{{ $category['title'] }}</div>
                            <div class="col-md-2 float-left category_action">
                                <a href="{{ asset('/category/edit/'.$category['id']) }}"><i class="fas fa-pencil-alt category_edit"></i></a>
                                <a href="{{ asset('/category/destroy/'.$category['id']) }}"><i class="far fa-times-circle category_delete"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div id="tags" class="tab-pane fade">
                <div class="col-lg-12"></div>
            </div>
        </div>

    </div>

@endsection
