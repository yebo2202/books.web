<?php

namespace App\Http\Controllers;

use App\Http\Models\AuthorModel;
use App\Http\Models\BookModel;
use App\Http\Models\CategoryModel;
use App\Http\Models\ExportImportModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class SettingController extends Controller
{
    public function index($link = 'general')
    {
        $authors = AuthorModel::where('user_id', Auth::id())->get();
        $categories = CategoryModel::where('user_id', Auth::id())->get();
        $export = ExportImportModel::where([
            ['user_id', Auth::id()],
            ['type_of_action', '1']
        ])->latest()->first();
        $import = ExportImportModel::where([
            ['user_id', Auth::id()],
            ['type_of_action', '2']
        ])->latest()->first();
        return view('settings.index', compact('authors', 'categories', 'export', 'import', 'link'));
    }

    public function export()
    {
        if (count(BookModel::get()->toArray()) < 1 ) {
            return redirect()->back()->with('message', 'Библиотека пустая. Экспорт не будет продолжен.');
        }
        // Создаю архив с файлами книг (PDF)
        $pdf_files = substr_replace(Storage::files('/public/'), '', 0, 7) ;
        if (file_exists('downloads/export_books.zip')) {
            unlink('downloads/export_books.zip');
        }

        $zip = new ZipArchive;
        if ($zip->open('downloads/export_books.zip', ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) === TRUE) {

            // Перебираю все файлы в /storage/* и добавляю их в архив
            foreach ($pdf_files as $key => $pdf_file) {
                if ($pdf_file != '.gitignore') {
                    $zip->addFile(public_path('/storage/').$pdf_file, '/files/'.$pdf_file);
                }
            }

            // Создаю массив с данными для экспорта.
            $books = BookModel::with('author')->select('id','description','filename','image','pages','param','title','year')->get()->toArray();
            if (File::put('downloads/database.txt', serialize($books))) {
                $zip->addFile(public_path('/downloads/database.txt'), '/db/database.txt');
            }
            $zip->close();

            // Запись в БД.
            $export = new ExportImportModel();
            $export->user_id = Auth::id();
            $export->type_of_action = '1';
            $export->save();
        }

        // Подчищаю файлы
        if (file_exists('downloads/database.txt')) unlink('downloads/database.txt');


        if(file_exists('downloads/export_books.zip')) {
            // Header
            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );
            Session::flash('message', 'Экспорт выполнен успешно');
//            Response::download('downloads/export_books.zip', 'export_books.zip', ['location' => '/setting']);
            response()->download('downloads/export_books.zip', 'export_books.zip', $headers);
//            return redirect('/setting')->with('message', 'Экспорт выполнен успешно');
        }

    }
}
