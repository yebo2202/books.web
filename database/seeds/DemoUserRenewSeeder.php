<?php

use App\Http\Models\AuthorModel;
use App\Http\Models\CategoryModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DemoUserRenewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('books')->where('user_id', 2)->delete();
        DB::table('authors')->where('user_id', 2)->delete();
        DB::table('categories')->where('user_id', 2)->delete();
        DB::table('queue')->where('user_id', 2)->delete();

        // Авторы
        $authors = [
            'Жюль Верн',
            'Артур Конан Дойл',
            'Александр Пушкин',
            'Лев Толстой'
        ];

        foreach ($authors as $item) {
            $author = new AuthorModel([
                'user_id' => 2,
                'name' => $item
            ]);
            $author->save();
        }

        // Категории
        $categories = [
            'Детектив',
            'История',
            'Классика',
            'Фантастика'
        ];

        foreach ($categories as $val) {
            $category = new CategoryModel([
                'user_id' => 2,
                'title' => $val
            ]);
            $category->save();
        }

    }
}
