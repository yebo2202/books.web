<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class AuthorModel extends Model
{
    public $table = 'authors';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'name'
    ];

    public function book()
    {
        return $this->belongsToMany('App\Http\Models\BookModel', 'book_author','author_id', 'book_id', 'id')->withTimestamps();
    }
}
