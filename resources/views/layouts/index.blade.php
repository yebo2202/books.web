@extends('layouts.app')

@section('content')

    <!-- Page Content -->
    <div class="container">

        <br>
        <div class="col-lg-12">
            @if (Session::has('message'))
                <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('message_danger'))
                <div class="alert alert-danger fs-13">{{ Session::get('message_danger') }}</div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <!-- Page Heading -->
        <div class="col-lg-3 my-4 fs-20">Главная
            <i class="fas fa-angle-right fs-20"></i>
            <small>Список книг</small>
        </div>

        <div id="app">
            <main-page-component
                    :values="{{ $books }}">
            </main-page-component>
        </div>

        <script src="{{ asset('/js/app.js') }}"></script>

    </div>

@endsection