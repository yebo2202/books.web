@extends('layouts.app')

@section('content')

    <div class="container">

        <br>

        <div class="col-lg-12">
            @if (Session::has('message'))
                <div class="alert alert-success fs-13">{{ Session::get('message') }}</div>
            @endif
            @if (Session::has('message_danger'))
                <div class="alert alert-danger fs-13">{{ Session::get('message_danger') }}</div>
            @endif
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>

        <div class="col-lg-3 my-4 fs-20">Книги
            <i class="fas fa-angle-right fs-20"></i>
            <small>Очереди</small>
        </div>

        @if( count($queues) > 0 )

            @foreach( $queues as $key => $queue )
                <div class="row">
                    <div class="col-lg-3">
                        <div class="queue-order">{{ $key + 1 }}</div>
                    </div>
                    <div class="col-lg-2">
                        <a href="{{ asset('/book/detail/'.$queue['book_id']) }}">
                            <img class="queue-image" src="/images/photos/{{ $queue['book']['image'] }}">
                        </a>
                    </div>
                    <div class="col-lg-5">
                        <div class="row fs-15">
                            <div class="col-lg-3">Название: </div>
                            <div class="col-lg-5 detail-book-title fs-16"><a href="{{ asset('/book/detail/'.$queue['book_id']) }}">{{ $queue['book']['title'] }}</a></div>
                        </div>
                        @if(count($queue['book']['category']) > 0)
                            <div class="row fs-15">
                                <div class="col-lg-3">Категория: </div>
                                <div class="col-lg-5 detail-book-title fs-16">{{ $queue['book']['category']['title'] }}</div>
                            </div>
                        @endif
                        @if(count($queue['author']) > 0)
                            <div class="row fs-15">
                                <div class="col-lg-3">Автор: </div>
                                <div class="col-lg-5 card-author">
                                    @foreach( $queue['author'] as $author )
                                        <a href="/author/1">{{ $author['name'] }}</a><br>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-1">
                        <a href="{{ asset('/queue/del/'.$queue['book_id']) }}"><button class="btn btn-outline-secondary">Удалить из очереди</button></a>
                    </div>
                </div>

                <hr>
            @endforeach

        @else
            <div class="col-lg-12 text-center fs-14">
                В очереди книг нет
            </div>
        @endif

    </div>

@endsection