##Books

####Описание
Приложение представляет собой локально развернутую библиотеку для хранения электронных книг.
Пока что поддерживается `только формат PDF`.

####Технологии

- laravel 5.5+
- mysql 5.7+
- php-gd
- vue.js

####Установка

1. `git clone https://github.com/YevgenBorshch/books`.
2. `composer update`.
3. Отредактировать файл `.env` для подключения к БД.
4. `php artisan migrate`.
5. Перейти по адресу настроенному в web сервере.
6. Залогиниться. Пользователь по-умолчанию `admin`, email `admin@localhost.com`, пароль `admin`.

####Настройка

- При загрузке больших pdf файлов могут возникнуть ошибки. 
Поэтому лучше сразу изменить значение переменных `post_max_size` и `upload_max_filesize` (я установил 500М) в файле `/etc/php/*/apache2/php.ini`.


##### Продолжение следует ... 