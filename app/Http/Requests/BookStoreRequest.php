<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'integer',
            'description' => 'present|min:0|max:1000',
            'filename' => 'file|max:255',
            'image' => 'file|max:255',
            'pages' => 'present|min:0|max:10',
            'title' => 'required|string|max:255',
            'year' => 'integer|min:2000',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Необходимо указать название книги',
            'title.max'  => 'Название должно быть не более 255 символов',
            'description.max' => 'Описание книги должно быть не более 1000 символов',
        ];
    }
}
