<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportImportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_import', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('type_of_action')->nullable()->comment('Тип события (1-экспорт, 2-импорт, 3-резервная копия)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_import');
    }
}
