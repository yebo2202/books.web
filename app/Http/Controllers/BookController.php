<?php

namespace App\Http\Controllers;

use App\Http\Models\AuthorModel;
use App\Http\Models\BookModel;
use App\Http\Models\CategoryModel;
use App\Http\Models\QueueModel;
use App\Http\Requests\BookStoreRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class BookController extends Controller
{
    // Главная страница

    public function index()
    {
        $books = BookModel::with('author','queue')->where('user_id', Auth::id())->get();
        if (Auth::id() == 2) {
            Session::flash('message_danger', 'Вы вошли под пользователем "demo". Вся сохраненная информация будет удалена ночью.');
        }
        return view('layouts.index', compact('books'));
    }

    // Страница с детальным описанием

    public function detail($id)
    {
        $book = BookModel::with('author', 'category')->where('id', $id)->get()->toArray();
        if (count($book) < 1) {
            return abort(404, 'Книга не найдена');
        }
        if ( $book['user_id'] == Auth::id()) {
            return view('books.detail', compact('book'));
        }
        return abort(404);
    }

    // Страница с формой для создания новой книги

    public function create()
    {
        $authors = AuthorModel::where('user_id', Auth::id())->get();
        $categories = CategoryModel::where('user_id', Auth::id())->get();
        return view('books.create', compact('authors', 'categories'));
    }

    // Редактирование книги

    public function edit($id)
    {
        $book = BookModel::where('id', $id)->with('author', 'category')->get();
        if ( $book['0']->user_id == Auth::id()) {
            $categories = CategoryModel::where('user_id', Auth::id())->get();
            $authors = AuthorModel::where('user_id', Auth::id())->get();
            return view('books.edit', compact('authors', 'book', 'categories'));
        }
        return abort(404);
    }

    // Обновление сохраненной книги

    public function update(BookStoreRequest $request, $id)
    {
        $book = BookModel::where('user_id', Auth::id())->find($id);

        // Обработка файла книги

        if ($request->file('file_book')) {
            if ($book['filename']  && file_exists('storage/'.$book['filename'])) {
                if(count(BookModel::where('filename', $book['filename'])->get()) == 1) {
                    // Удаляю файл с книгой с диска ТОЛЬКО при условии
                    // что файл существует на диске
                    // и он единственный в таблице (его не использует другая книга).
                    unlink('storage/' . $book['filename']);
                }
            }
            // Обновляю запись в БД.
            $book->update([
                'filename' => $this->file_book($request)
            ]);
        }

        // Обработка файла с главным изображением

        if ($request->file('file_image')) {
            if ($book['image'] != 'no_image.png' && file_exists('images/photos/'.$book['image'])) {
                if(count(BookModel::where('image', $book['image'])->get()) == 1) {
                    // Удаляю файл с изображением с диска ТОЛЬКО при условии
                    // что файл существует на диске
                    // и он единственный в таблице (его не использует другая книга).
                    unlink('images/photos/'.$book['image']);
                }
            }
            // Обновляю запись в БД.
            $book->update([
                'image' => $this->file_image($request)
            ]);
        }

        // Таблица "books"

        $book->update([
            'title' => $request['title'],
            'category_id' => $request['category_id'],
            'description' => $request['description'],
            'pages' => $request['pages'],
            'year' => $request['year'],
            'updated_at' => Carbon::now(),
        ]);

        // Таблица "authors"

        $author = new AuthorModel();
        $book->author()->detach();
        $book->author()->attach($request->author_exist);
        if (isset($request['author_new']) && count($request->author_new) > 0) {
            foreach ($request->author_new as $item) {
                $author->user_id = Auth::id();
                $author->name = $item;
                $author->save();
                $last_author_id = $author->id;
                $book->author()->attach( $id , ['author_id' => $last_author_id]);
            }
        }
        return redirect('/')->with('message', 'Книга успешно обновлена');
    }

    // Сохранение новой книги

    public function store(BookStoreRequest $request)
    {

        // Создаю экземпляр модели

        $book = new BookModel();
        $author = new AuthorModel();

        // Обработка файла с главным изображением

        if ($request->file('file_image')) {
            $file_image = $this->file_image($request);
        } else {
            $file_image = 'no_image.png';
        }

        // Обработка файла книги

        if ($request->file('file_book')) {
            $file_book = $this->file_book($request);
            $book->filename = $file_book;
        }

        // Таблица "books"

        $book->title = $request['title'];
        $book->category_id = $request['category_id'];
        $book->user_id = Auth::id();
        $book->description = $request['description'];
        $book->image = $file_image;
        $book->pages = $request['pages'];
        $book->title = $request['title'];
        $book->year = $request['year'];
        $book->save();
        $last_book_id = $book->id;

        // Таблица "authors"

        if ( isset($request['author_new']) && count($request['author_new']) > 0 ) {
            foreach ($request['author_new'] as $item) {
                $author->user_id = Auth::id();
                $author->name = $item;
                $author->save();
                $last_author_id = $author->id;
                $book->author()->attach( $last_book_id, [ 'author_id' => $last_author_id ] );
            }
        } else {
            if ( count($request['author_exist']) > 0 ) {
                foreach ($request['author_exist'] as $item) {
                    $book->author()->attach($last_book_id, ['author_id' => $item]);
                }
            }
        }

        return redirect('/')->with('message', 'Книга успешно добавлена');
    }

    // Удаление книги

    public function destroy($id)
    {
        $book = BookModel::find($id);
        if ( $book->user_id == Auth::id()) {

            // Удаляю изображение книги, если это не изображение по-умолчанию.

            if ($book['image'] != 'no_image.png') {
                unlink('images/photos/'.$book['image']);
            }

            // Удаляю файл книги.

            if ($book['filename']) {
                unlink('storage/'.$book['filename']);
            }

            $book->author()->detach();
            BookModel::where('id', $id)->delete();
            QueueModel::where('book_id', $id)->delete();
            return redirect('/')->with('message', 'Книга успешно удалена');
        }
        return abort(404);
    }

    // Метод обрабатывает файл с изображением книги.
    // Возвращает имя сохраненного файла для записи в БД.

    public function file_image(Request $request)
    {
        // Обработка файла с изображением книги.

        $file = $request->file('file_image');
        Image::configure(array('driver' => 'gd'));

        // Создание объекта с изображением.

        $image_book = Image::make($file->getRealPath());

        // Новое имя для файла - чтобы все файлы были названы по шаблону.

        $hash = md5_file($file->getRealPath());

        // Имя файла без префикса, для БД.

        $file_image = $hash.'.'.'jpg';

        // Перемещение файла с изображением.

        $destinationPath = 'images/photos';

        // Сохраняю файл в папке с изображениями.

        $image_book->save($destinationPath.'/'.$file_image);

        // Возвращаю имя сохраненного файла.

        return $file_image;
    }

    public function file_book(Request $request)
    {
        // Обработка файла книги.

        $file = $request->file('file_book');

        // Новое имя для файла - чтобы все файлы были названы по шаблону.

        $hash = md5_file($file->getRealPath());

        // Имя файла без префикса, для БД.

        $file_book = $hash.'.'.'pdf';

        // Перемещение файла.

        $request->file('file_book')->storeAs('public', $file_book);

        // Возвращаю имя сохраненного файла.

        return $file_book;
    }

    public function read($id, Request $request)
    {

        // Получаю книгу по требуемому id.
        $book = BookModel::find($id);

        // Если книга пренадлежит текущему пользователю, то возвращаю ее.
        if ( $book->user_id == Auth::id()) {

            // Передаю параметры запрошенной книги просмотрщику.
            return view('viewer.read', compact('book'));
        }
        return abort(404);
    }

    public function param(Request $request)
    {

        // Обновляю запись в БД.
        $book = DB::table('books')
            ->where('id', $request->book_id)
            ->update([
                'param' => $request->param,
                'pages' => $request->pages
                ]);

        if ($book) {
            return $request->json('data', ['success']);
        }

        return $request->json('data', ['error']);
    }
}
